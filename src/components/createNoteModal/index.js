import { Modal } from "../modal";
import { useEffect, useRef, useState } from "react";
import SaveIcon from "@mui/icons-material/Save";
import { StyledPrimaryButton } from "./../../styles";
import { CreateNote, CreateNoteBody, CreateNoteFooter, CreateNoteHeader } from "./style";

export const CreateNoteModal = ({ show, setShow, title }) => {
  const [notes, setNotes] = useState(
    localStorage.getItem("notes") ? JSON.parse(localStorage.getItem("notes")) : []
  );
  const [notetitle, setNoteTitle] = useState("");
  const [content, setContent] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!notetitle || !content) {
      alert("Please fill all the fields");
      return;
    }

    if (notes.length < 1) {
      setNotes([
        {
          title: notetitle,
          content: content,
          id: notes.length + 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } else {
      setNotes([
        ...notes,
        {
          title: notetitle,
          content: content,
          id: notes.length + 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    }
    setContent("");
    setNoteTitle("");
    setTimeout(() => {
      setShow(false);
    }, 200);
  };
  useEffect(() => {
    window.localStorage.setItem("notes", JSON.stringify(notes));
  }, [notes]);

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, [show]);
  return (
    <>
      {title ? (
        <Modal show={show} setShow={setShow} title={title}>
          <CreateNote onSubmit={handleSubmit}>
            <CreateNoteHeader>
              <input
                value={notetitle}
                onChange={(e) => setNoteTitle(e.target.value)}
                placeholder="Title"
                ref={inputRef}
              />
            </CreateNoteHeader>
            <CreateNoteBody>
              <textarea
                value={content}
                onChange={(e) => setContent(e.target.value)}
                placeholder="Write your note here..."
              />
            </CreateNoteBody>
            <CreateNoteFooter>
              <StyledPrimaryButton className="btn" type="submit">
                <SaveIcon />
              </StyledPrimaryButton>
            </CreateNoteFooter>
          </CreateNote>
        </Modal>
      ) : null}
    </>
  );
};
