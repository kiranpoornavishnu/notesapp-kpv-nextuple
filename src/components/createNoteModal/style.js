import styled from "styled-components";

export const CreateNote = styled.form`
  display: flex;
  flex-direction: column;
  width: 800px;

  @media only screen and (max-width: 940px) {
    width: 600px;
  }

  @media only screen and (max-width: 720px) {
    width: 400px;
  }

  @media only screen and (max-width: 520px) {
    width: 250px;
  }

 
`;

export const CreateNoteHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 10px;

  input {
    border: none;
    outline: none;
    font-size: 32px;
    font-weight: 400;
    width: 100%;
  }
`;

export const CreateNoteBody = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  textarea {
    border: none;
    outline: none;
    font-size: 18px;
    font-weight: 400;
    width: 100%;
    height: 400px;
    resize: none;

    @media only screen and (max-width: 720px) {
      height: 300px;
    }

    @media only screen and (max-width: 520px) {
      height: 200px;
    }
    
  }
`;

export const CreateNoteFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding-top: 10px;
`;
