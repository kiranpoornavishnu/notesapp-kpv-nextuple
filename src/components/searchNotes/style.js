import styled from "styled-components";

export const StyledSearchNotes = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: 0 20px;

  input {
    height: 30px;
    border-radius: 40px;
    padding: 0 0 0 18px;
  }

  button {
    display: none;
  }
`;

export const StyledSearchNotesResult = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 20px;
  gap: 20px;
`;
