import { useEffect, useState } from "react";
import { NoteCard } from "../noteCard";
import { StyledSearchNotes, StyledSearchNotesResult } from "./style";

export const SearchNotes = ({ notes, setNotes }) => {
  const [search, setSearch] = useState("");
  const [searchResult, setSearchResult] = useState(notes ? notes : []);
  const [updateNoteModal, setUpdateNoteModal] = useState(false);

  const handleSearch = (e) => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    const debounce = setTimeout(() => {
      const res = notes.filter((note) => {
        return note.title.toLowerCase().includes(search.toLowerCase());
      });
      setSearchResult(res);

      console.log("debounce");
    }, 400);

    return () => {
      clearTimeout(debounce);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  return (
    <>
      <StyledSearchNotes>
        <form>
          <input
            type="text"
            placeholder="Search Notes"
            aria-label="Search Notes"
            value={search}
            onChange={handleSearch}
          />
          <button type="submit" title="submit btn">
            Search
          </button>
        </form>
      </StyledSearchNotes>
      <StyledSearchNotesResult>
        {searchResult.map((note) => (
          <NoteCard
            note={note}
            updateNoteModal={updateNoteModal}
            setUpdateNoteModal={setUpdateNoteModal}
          />
        ))}
      </StyledSearchNotesResult>
    </>
  );
};
