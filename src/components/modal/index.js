import React, { useEffect } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { ModalWrapper, ModalContent, ModalHeader, ModalBody } from "./style";
import { StyledPrimaryButton } from "../../styles";

export const Modal = ({ children, show, setShow, title }) => {
  const handleClose = () => {
    if (
      window.confirm(
        "Your changes are not saved! \nAre you sure you want to close?"
      )
    ) {
      setShow(false);
    }
  };
  useEffect(() => {
    if (show) {
      document.body.style.overflow = "hidden";
    }
    return () => {
      document.body.style.overflow = "unset";
    };
  }, [show]);
  return (
    <>
      {show && (
        <ModalWrapper>
          <ModalContent>
            <ModalHeader>
              <h2>{title ? title : null}</h2>
              <StyledPrimaryButton onClick={handleClose}>
                <CloseIcon />
              </StyledPrimaryButton>
            </ModalHeader>
            <ModalBody>{children}</ModalBody>
          </ModalContent>
        </ModalWrapper>
      )}
    </>
  );
};
