import { Modal } from "../modal";
import { useEffect, useState } from "react";
import InfoIcon from "@mui/icons-material/Info";
import DeleteIcon from "@mui/icons-material/Delete";
import SaveIcon from "@mui/icons-material/Save";
import {
  StyledUpadteNote,
  StyledUpadteNoteBody,
  StyledUpadteNoteFooter,
  StyledUpadteNoteHeader,
} from "./style";
import { StyledPrimaryButton } from "../../styles";

export const UpdateNoteModal = ({ show, setShow, title, note }) => {
  const [notes, setNotes] = useState(
    localStorage.getItem("notes")
      ? JSON.parse(localStorage.getItem("notes"))
      : []
  );

  const [notetitle, setNoteTitle] = useState(note ? note.title : "");
  const [content, setContent] = useState(note ? note.content : "");

  const updatedAt = note
    ? `Updated at: ${note.updatedAt.substring(0, 10)} ${new Date(
        note.updatedAt
      ).getHours()}:${new Date(note.updatedAt).getMinutes()}:${new Date(
        note.updatedAt
      ).getSeconds()}`
    : "";
  const createdAt = note
    ? `Created at: ${note.createdAt.substring(0, 10)} ${new Date(
        note.createdAt
      ).getHours()}:${new Date(note.createdAt).getMinutes()}:${new Date(
        note.createdAt
      ).getSeconds()}`
    : "";

  const handleSubmit = (e) => {
    e.preventDefault();

    notes.find((item) => item.id === note.id).title = notetitle;
    notes.find((item) => item.id === note.id).content = content;
    notes.find((item) => item.id === note.id).updatedAt = new Date();

    setTimeout(() => {
      setShow(false);
    }, 200);
    localStorage.setItem("notes", JSON.stringify(notes));
  };
  const handleDelete = () => {
    if (window.confirm("Are you sure you want to delete this note?")) {
      const newNotes = notes.filter((item) => item.id !== note.id);
      setNotes(newNotes);
      localStorage.setItem("notes", JSON.stringify(newNotes));
      setTimeout(() => {
        setShow(false);
      }, 200);
    } else {
      // do nothing
      return;
    }
  };
  useEffect(() => {
    window.localStorage.setItem("notes", JSON.stringify(notes));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {note && (
        <Modal show={show} setShow={setShow} title={title}>
          <StyledUpadteNote onSubmit={handleSubmit}>
            <StyledUpadteNoteHeader>
              <input
                value={notetitle}
                onChange={(e) => setNoteTitle(e.target.value)}
                placeholder="Title"
              />
            </StyledUpadteNoteHeader>
            <StyledUpadteNoteBody>
              <textarea
                value={content}
                onChange={(e) => setContent(e.target.value)}
                placeholder="Note"
              />
            </StyledUpadteNoteBody>
            <StyledUpadteNoteFooter>
              <div>
                <StyledPrimaryButton
                  type="button"
                  title={`${updatedAt}\n${createdAt}`}
                  color="gray"
                  background="white"
                  name="info"
                >
                  <InfoIcon fontSize="large" />
                </StyledPrimaryButton>
                <StyledPrimaryButton type="button" onClick={handleDelete}>
                  <DeleteIcon fontSize="small" />
                </StyledPrimaryButton>
              </div>

              <StyledPrimaryButton type="submit">
                <SaveIcon />
              </StyledPrimaryButton>
            </StyledUpadteNoteFooter>
          </StyledUpadteNote>
        </Modal>
      )}
    </>
  );
};
