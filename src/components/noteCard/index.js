import { useEffect, useState } from "react";
import { UpdateNoteModal } from "../updateNoteModal";
import { StyledNoteCard } from "./style";

export const NoteCard = ({ note, updateNoteModal, setUpdateNoteModal }) => {
  const [show, setShow] = useState(false);

  const handleUpdateNote = () => {
    setShow(true);
    setUpdateNoteModal(true);
  };
  useEffect(() => {
    setUpdateNoteModal(show);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [show]);

  return (
    <>
      <StyledNoteCard onClick={handleUpdateNote}>
        <div>{note?.title}</div>
      </StyledNoteCard>
      {show && (
        <UpdateNoteModal
          setShow={setShow}
          show={show}
          note={note}
          title={"Update Note"}
        />
      )}
    </>
  );
};
