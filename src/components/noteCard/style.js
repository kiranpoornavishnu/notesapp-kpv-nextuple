import styled from "styled-components";

export const StyledNoteCard = styled.div`
  display: flex;
  flex-direction: column;
  width: 240px;
  max-height: 120px;
  padding: 10px;
  border: 1px solid #e6e6e6;
  border-radius: 5px;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.1);
  background-color: #fff;
  cursor: pointer;

  div {
    font-size: 24px;
    font-weight: 600;
    margin: 0;
    margin-bottom: 10px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;
