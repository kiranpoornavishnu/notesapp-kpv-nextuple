import styled from "styled-components";

export const StyledHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  background: #0b0b4f;
  //border-radius: 0 0 25px 25px;
  padding: 28px 50px;
`;

export const StyledButton = styled.button`
  padding: 8px 16px;
  border-radius: 25px;
  font-size: 16px;
  font-weight: 500;
  cursor: pointer;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const StyledPrimaryButton = styled.button`
  background: ${({ background }) => background || "#0b0b4f"};
  border: none;
  cursor: pointer;
  font-size: 24px;
  color: ${({ color }) => color || "#fff"};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${({ name }) => (name === "info" ? "0" : "6px 10px")};
  border-radius: 5px;
`;

export const StyledNotesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 20px;
  gap: 20px;
`;

export const StyledSelectWrapper = styled.div`
  display: flex;
  align-items: center;
  background: white;
  border-radius: 25px;
  padding: 8px 16px;
  position: relative;
  width: 150px;
  justify-content: end;
  cursor: pointer;

  select {
    appearance: none;
    position: absolute;
    background-color: transparent;
    border: none;
    padding: 0 0 0 16px;
    margin: 0;
    width: 100%;
    font-family: inherit;
    font-size: inherit;
    cursor: inherit;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 1;
    outline: none;
  }
`;
