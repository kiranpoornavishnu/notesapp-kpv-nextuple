import { useEffect, useState } from "react";

import { NoteCard } from "./components/noteCard";
import { CreateNoteModal } from "./components/createNoteModal/index";
import { SearchNotes } from "./components/searchNotes";
import {
  StyledHeaderWrapper,
  StyledNotesWrapper,
  StyledButton,
  StyledSelectWrapper,
} from "./styles";
import TuneIcon from "@mui/icons-material/Tune";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

function App() {
  const [notes, setNotes] = useState([]);
  const [addNoteModal, setAddNoteModal] = useState(false);
  const [updateNoteModal, setUpdateNoteModal] = useState(false);
  const [sort, setSort] = useState(
    localStorage.getItem("sort") ? localStorage.getItem("sort") : "fcreatedBy"
  );
  const [search, setSearch] = useState(false);

  const handleAddNote = () => {
    setAddNoteModal(true);
  };
  useEffect(() => {
    setNotes(
      localStorage.getItem("notes")
        ? JSON.parse(localStorage.getItem("notes"))
        : []
    );
    localStorage.setItem("sort", sort);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [localStorage.getItem("notes"), updateNoteModal, sort]);

  const sortBy = {
    fcreatedBy: (a, b) => new Date(a.createdAt) - new Date(b.createdAt),
    lcreatedBy: (a, b) => new Date(b.createdAt) - new Date(a.createdAt),
    fupadatedBy: (a, b) => new Date(a.updatedAt) - new Date(b.updatedAt),
    lupadatedBy: (a, b) => new Date(b.updatedAt) - new Date(a.updatedAt),
  };

  return (
    <div>
      <StyledHeaderWrapper>
        {search ? (
          <StyledButton onClick={() => setSearch(false)}>
            <ArrowBackIcon />
          </StyledButton>
        ) : (
          <StyledButton onClick={handleAddNote}>Add Notes</StyledButton>
        )}

        <div>
          {!search && notes.length > 0 && (
            <StyledButton
              onClick={() => {
                setSearch(true);
              }}
            >
              Search
            </StyledButton>
          )}
        </div>
        <StyledSelectWrapper>
          <select
            value={`${sort}`}
            onChange={(e) => {
              setSort(e.target.value);
            }}
          >
            <option value="fcreatedBy">Oldest</option>
            <option value="lcreatedBy">Newest</option>
            <option value="fupadatedBy">Fisrt Updated By</option>
            <option value="lupadatedBy">Last Updated By</option>
          </select>
          <div>
            <TuneIcon />
          </div>
        </StyledSelectWrapper>
      </StyledHeaderWrapper>

      <StyledNotesWrapper>
        {notes.length > 0 && !search ? (
          notes
            ?.sort((a, b) => {
              const sortKey = sortBy[sort];
              return sortKey(a, b);
            })
            .map((note) => (
              <>
                <NoteCard
                  note={note}
                  updateNoteModal={updateNoteModal}
                  setUpdateNoteModal={setUpdateNoteModal}
                />
              </>
            ))
        ) : notes.length > 0 && search ? (
          <SearchNotes notes={notes} />
        ) : (
          <div>No Notes Found</div>
        )}
      </StyledNotesWrapper>
      {addNoteModal && (
        <CreateNoteModal
          show={addNoteModal}
          setShow={setAddNoteModal}
          title={"Create Note"}
        />
      )}
    </div>
  );
}

export default App;
